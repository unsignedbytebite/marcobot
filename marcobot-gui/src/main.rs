mod boot;
mod controller;
mod style;

use boot::Boot;
use controller::Controller;
use macroquad::prelude::*;

#[macroquad::main("marcobot-v2")]
async fn main() {
    let mut control = None;
    let mut splash = Boot::new(load_texture("assets/marcobot2.png").await.unwrap());
    let scanlines = load_texture("./assets/scanlines.png").await.unwrap();

    loop {
        if is_key_down(KeyCode::Escape) {
            break;
        }

        match &mut control {
            None => {
                if splash.pulse() {
                    control = Some(Controller::new(
                        load_texture("assets/bot.png").await.unwrap(),
                        load_texture("assets/his.png").await.unwrap(),
                    ));
                }
            }
            Some(c) => {
                c.pulse();
            }
        }

        draw_texture(
            &scanlines,
            0.0,
            0.0,
            Color {
                a: 0.2,
                ..Default::default()
            },
        );

        next_frame().await
    }
}
