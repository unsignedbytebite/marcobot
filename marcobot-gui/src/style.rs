use macroquad::color::Color;

pub const COLOUR_ACCENT: Color = Color {
    r: 1.0,
    g: 0.949,
    b: 0.875,
    a: 1.0,
};
pub const COLOUR_BASE: Color = Color {
    r: 0.937,
    g: 0.141,
    b: 0.227,
    a: 1.0,
};
