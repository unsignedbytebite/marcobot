use macroquad::prelude::*;
use marcobot_protocol::{Command, Direction, Speed};
use std::{io::Write, net::TcpStream};

use crate::style;

const ROT_SPEED: f32 = 1.0;
const ACC_SCALE: f32 = 8.0;
const HISTORY_TIMEOUT_DELTA: f32 = 1.0;
const SERVER_IP: &str = include_str!("../../controller_server_ip.txt");
const MAX_MARKERS: usize = 128;
const CONNECTION_TIMEOUT: f32 = 2.0;

pub struct Controller {
    bot_pos: Vec2,
    bot_rot: f32,
    speed: f32,
    history_markers: Vec<Option<Vec2>>,
    history_head: usize,
    history_timeout: f32,
    stream: Option<TcpStream>,
    command: Command,
    set_speed: Speed,
    texture_bot: Texture2D,
    texture_his: Texture2D,
    connection_timeout: f32,
}

impl Controller {
    pub fn new(texture_bot: Texture2D, texture_his: Texture2D) -> Self {
        let bot_pos = vec2(screen_width() / 2.0, screen_height() / 2.0);
        let bot_rot = 0.0;
        let speed = 1.0;
        let history_markers: Vec<Option<Vec2>> = vec![None; MAX_MARKERS];
        let history_head = 0;
        let history_timeout = 0.0;
        // let stream = TcpStream::connect(SERVER_IP).expect("Cannot connect to: ");
        let stream = None;
        let command = Command::Stop;
        let set_speed = Speed::Normal;
        let connection_timeout = 0.0;

        Self {
            bot_pos,
            bot_rot,
            speed,
            history_markers,
            history_head,
            history_timeout,
            stream,
            command,
            set_speed,
            texture_bot,
            texture_his,
            connection_timeout,
        }
    }

    pub fn pulse(&mut self) {
        match &mut self.stream {
            Some(stream) => {
                let mut error = false;

                let mut set_command = |cmd| {
                    if self.command != cmd {
                        self.command = cmd;
                        let buffer = [self.command.to_byte(); 1];
                        if stream.write(&buffer).is_err() {
                            error = true;
                        }
                    }
                };

                if is_key_down(KeyCode::Key1) {
                    self.speed = 1.0;
                    self.set_speed = Speed::Slow;
                } else if is_key_down(KeyCode::Key2) {
                    self.speed = 2.0;
                    self.set_speed = Speed::Normal;
                } else if is_key_down(KeyCode::Key3) {
                    self.speed = 3.0;
                    self.set_speed = Speed::Fast;
                } else if is_key_released(KeyCode::D)
                    || is_key_released(KeyCode::A)
                    || is_key_released(KeyCode::W)
                {
                    set_command(Command::Stop);
                } else if is_key_down(KeyCode::D) {
                    self.bot_rot += ROT_SPEED * get_frame_time();
                    set_command(Command::Go(self.set_speed.clone(), Direction::Right));
                } else if is_key_down(KeyCode::A) {
                    self.bot_rot -= ROT_SPEED * get_frame_time();
                    set_command(Command::Go(self.set_speed.clone(), Direction::Left));
                } else if is_key_down(KeyCode::W) {
                    let rot_vec = Vec2::new(self.bot_rot.sin(), -self.bot_rot.cos());
                    self.bot_pos += rot_vec * get_frame_time() * self.speed * ACC_SCALE;
                    set_command(Command::Go(self.set_speed.clone(), Direction::Forward));

                    self.history_timeout += HISTORY_TIMEOUT_DELTA * get_frame_time();
                    if self.history_timeout > 1.0 {
                        self.history_timeout = 0.0;

                        self.history_head += 1;
                        if self.history_head > self.history_markers.len() - 1 {
                            self.history_head = 0;
                        }

                        self.history_markers[self.history_head] = Some(self.bot_pos);
                    }
                }

                if error{
                    self.stream = None;
                }

                clear_background(style::COLOUR_BASE);

                for h in self.history_markers.iter().flatten() {
                    draw_texture(&self.texture_his, h.x - 3.0, h.y - 3.0, WHITE);
                }

                draw_texture_ex(
                    &self.texture_bot,
                    self.bot_pos.x - 16.0,
                    self.bot_pos.y - 16.0,
                    WHITE,
                    DrawTextureParams {
                        rotation: self.bot_rot,
                        ..Default::default()
                    },
                );
            }
            None => {
                self.connection_timeout += get_frame_time();

                if self.connection_timeout > CONNECTION_TIMEOUT {
                    self.connection_timeout = 0.0;

                    let stream = TcpStream::connect(SERVER_IP);
                    match stream {
                        Ok(stream) => self.stream = Some(stream),
                        Err(_) => self.stream = None,
                    }
                }

                clear_background(style::COLOUR_ACCENT);
                draw_text(
                    "Attempting to connect...",
                    screen_width() / 2.0,
                    screen_height() / 2.0,
                    32.0,
                    style::COLOUR_BASE,
                );
            }
        }
    }
}
