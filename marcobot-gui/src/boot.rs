use macroquad::{
    color::{BLACK,  WHITE},
    shapes::{draw_rectangle, draw_rectangle_lines},
    text::draw_text,
    texture::{draw_texture,  Texture2D },
    time::get_frame_time,
    window::{clear_background, screen_height, screen_width}
};

use crate::style;

pub struct Boot {
    counter: f32,
    texture: Texture2D,
}

const BOOT_MESSAGE: &[(&str, f32)] = &[
    ("[ marco_drivers.dfc ]", 0.1),
    ("[ os_start.pyy ]", 0.2),
    ("[ de_nata.tart ]", 0.5),
    ("[ eng.dic ]", 0.6),
    ("[ pt.dic ]", 0.7),
    ("[ meme.dic ]", 0.9),
    ("[ OK ]", 2.2),
];

const LOAD_TIME: f32 = 3.0;
const LOAD_MESSAGE: &[(&str, f32, f32)] = &[
    ("boiling noodles", 3.0, 0.1),
    ("embracing nostalgia", 3.2, 0.3),
    ("permutating BAR outcomes", 3.8, 0.55),
    ("complaining about weather", 4.4, 0.6),
    ("building all linux distros", 4.9, 0.7),
    ("generating QUICK questions", 5.0, 0.99),
    ("complete", 9.0, 1.0),
];
const COMPLETE: f32 = 10.0;

impl Boot {
    pub fn new(texture: Texture2D) -> Self {
        Self {
            counter: 0.0,
            texture,
        }
    }

    pub fn pulse(&mut self) -> bool {
        self.counter += get_frame_time();

        if self.counter < LOAD_TIME {
            clear_background(BLACK);

            for (i, (s, t)) in BOOT_MESSAGE.iter().enumerate() {
                if self.counter > *t {
                    draw_text(s, 8.0, 16.0 + (i as f32 * 16.0), 16.0, WHITE);
                }
            }
            false
        } else if self.counter < COMPLETE {
            clear_background(style::COLOUR_BASE);

            draw_texture(
                &self.texture,
                screen_width() / 2.0 - (128.0 / 2.0),
                (screen_height() / 2.0) - 80.0,
                WHITE,
            );

            draw_text(
                "marcobot v2.0",
                0.0,
                screen_height(),
                16.0,
                style::COLOUR_ACCENT,
            );
            draw_rectangle_lines(
                100.0,
                (screen_height() / 2.0) + 20.0,
                screen_width() - 200.0,
                20.0,
                2.0,
                style::COLOUR_ACCENT,
            );

            let mut progress = 0.0;
            let mut msg = "";

            for (s, t, p) in LOAD_MESSAGE {
                if self.counter > *t {
                    msg = s;
                    progress = *p;
                }
            }

            draw_text(msg, 100.0, (screen_height() / 2.0) + 60.0, 16.0, style::COLOUR_ACCENT);

            draw_rectangle(
                100.0,
                (screen_height() / 2.0) + 20.0,
                (screen_width() - 200.0) * progress,
                20.0,
                style::COLOUR_ACCENT,
            );
            false
        } else {
            true
        }
    }
}
