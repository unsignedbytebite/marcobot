use crossterm::event::PushKeyboardEnhancementFlags;
use marcobot_protocol::{Command, Direction, Speed};
use ratatui::prelude::*;
use ratatui::prelude::{CrosstermBackend, Rect, Terminal};
use ratatui::widgets::{Block, Borders, Paragraph};
use std::{io::Write, net::TcpStream};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    crossterm::terminal::enable_raw_mode()?;
    crossterm::execute!(std::io::stderr(), crossterm::terminal::EnterAlternateScreen)?;

    let mut terminal = Terminal::new(CrosstermBackend::new(std::io::stderr()))?;
    let mut command = Command::Stop;
    let mut set_speed = Speed::Normal;

    const SERVER_IP: &str = include_str!("../../controller_server_ip.txt");
    let mut stream = TcpStream::connect(SERVER_IP).unwrap();

    loop {
        terminal.draw(|f| {
            f.render_widget(Paragraph::new("!-[ MarcoBot v0.1 ]-!\n\n- press ESC to exit -\n- press ARROWS to move -\n- press 1-3 to set speed -".to_string()).alignment(Alignment::Center).bold(), f.size());

            let p =Paragraph::new("ONLINE").light_green().not_bold().rapid_blink().block(Block::default().title("Connection").borders(Borders::ALL).white().not_rapid_blink());
            f.render_widget(p, Rect::new(0, 6, 12, 3));

            let speed_text = ">".repeat(set_speed.to_byte() as usize);
            let p =Paragraph::new(speed_text).block(Block::default().title("Speed").not_bold().borders(Borders::ALL));
            f.render_widget(p, Rect::new(12, 6, 8, 3));

            let direction_text = match &command{
                Command::Go(_speed, direction) =>  direction.to_string(),
                Command::Stop => "----".to_string(),
            };

            let p =Paragraph::new(direction_text).not_bold().block(Block::default().title("Direction").borders(Borders::ALL).white().not_rapid_blink());
            f.render_widget(p, Rect::new(8+12, 6, 12, 3));
        })?;

        let mut set_command = |dir, speed| {
            let set = match &command {
                Command::Go(_speed, direction) => *direction != dir,
                Command::Stop => true,
            };

            if set {
                command = Command::Go(speed, dir);
                let buffer = [command.to_byte(); 1];
                let _ = stream.write(&buffer).unwrap();
            }
        };

        if crossterm::event::poll(std::time::Duration::from_millis(250))? {
            if let crossterm::event::Event::Key(key) = crossterm::event::read()? {
                if key.kind == crossterm::event::KeyEventKind::Release {
                } else if key.kind == crossterm::event::KeyEventKind::Press {
                    match key.code {
                        crossterm::event::KeyCode::Char('1') => {
                            set_speed = Speed::Slow;
                        }
                        crossterm::event::KeyCode::Char('2') => {
                            set_speed = Speed::Normal;
                        }
                        crossterm::event::KeyCode::Char('3') => {
                            set_speed = Speed::Fast;
                        }
                        crossterm::event::KeyCode::Left => {
                            set_command(Direction::Left, set_speed.clone())
                        }
                        crossterm::event::KeyCode::Right => {
                            set_command(Direction::Right, set_speed.clone())
                        }
                        crossterm::event::KeyCode::Up => {
                            set_command(Direction::Forward, set_speed.clone())
                        }
                        crossterm::event::KeyCode::Enter => {
                            if command != Command::Stop {
                                command = Command::Stop;
                                let buffer = [command.to_byte(); 1];
                                let _ = stream.write(&buffer).unwrap();
                            }
                        }
                        crossterm::event::KeyCode::Esc => break,
                        _ => {}
                    }
                }
            }
        }
    }

    crossterm::execute!(
        std::io::stderr(),
        crossterm::terminal::LeaveAlternateScreen,
        PushKeyboardEnhancementFlags(
            crossterm::event::KeyboardEnhancementFlags::REPORT_EVENT_TYPES,
        )
    )?;
    crossterm::terminal::disable_raw_mode()?;

    Ok(())
}
