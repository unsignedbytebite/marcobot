use easy_parallel::Parallel;
use marcobot_protocol::Command;
use std::{io::prelude::*, net::TcpListener, sync::atomic::AtomicU8, time::Duration};

fn main() {
    let command_byte = AtomicU8::new(0);
    const REFRESH_RATE: Duration = Duration::from_millis(1000 / 60);

    const ROBOT_IP: &str = "0.0.0.0:7878";
    const MARCO_IP: &str = "0.0.0.0:7979";
    const CONNECTION_TEST: &str = "0.0.0.0:8080";

    let to_robot = || {
        smol::block_on(async {
            let listener = TcpListener::bind(ROBOT_IP).unwrap();

            for stream in listener.incoming() {
                let stream = &mut stream.unwrap();
                println!("-->> marcobot connected");
                loop {
                    let c = command_byte.fetch_and(0, std::sync::atomic::Ordering::Relaxed);

                    if c != 0 {
                        if stream.write_all(&[c]).is_err() {
                            println!("-->> marcobot disconnected");
                            break;
                        }
                        println!("--> marcobot command {c}");
                    }
                    std::thread::sleep(REFRESH_RATE);
                }
            }
        })
    };

    let from_marco = || {
        smol::block_on(async {
            let listener = TcpListener::bind(MARCO_IP).unwrap();

            loop {
                for stream in listener.incoming() {
                    let stream = &mut stream.unwrap();
                    println!("-->> marco connected");
                    loop {
                        let mut buffer = [0; 1];
                        match stream.read(&mut buffer) {
                            Ok(_) => {
                                if buffer[0] > 0 {
                                    let command = Command::from_byte(buffer[0]).unwrap();
                                    println!("--> marco {:?}", command);
                                    command_byte
                                        .store(buffer[0], std::sync::atomic::Ordering::Relaxed);
                                } else {
                                    println!("<<-- marco disconected");
                                    break;
                                }
                            }
                            Err(_) => {
                                println!("<<-- marco disconected");
                                break;
                            }
                        }
                        std::thread::sleep(REFRESH_RATE);
                    }
                }
                std::thread::sleep(REFRESH_RATE);
            }
        })
    };

    let from_test = || {
        smol::block_on(async {
            let listener = TcpListener::bind(CONNECTION_TEST).unwrap();

            loop {
                for _ in listener.incoming() {
                    println!("-->> test connected");
                }
                std::thread::sleep(REFRESH_RATE);
            }
        })
    };
    let _ = Parallel::new()
        .add(from_marco)
        .add(to_robot)
        .add(from_test)
        .run();

    loop {}
}
