use marcobot_protocol::{Command, Direction, Speed};

//PWM
use rppal::{pwm::{Channel, Polarity, Pwm, self}, gpio::Gpio};

//H-Bridge global configuration
const FREQUENCY: f64 = 5000.0; // 5kHz
const DUTY_20: f64 = 0.2;
const DUTY_30: f64 = 0.3;
const DUTY_40: f64 = 0.4;
const DUTY_60: f64 = 0.6;
const OFF: f64 = 0.0;

// GPIO Def
const GPIO_DIRECTION_M1: u8 = 6;// PHYSICAL 31
const GPIO_DIRECTION_M2: u8 = 5;// PHYSICAL 29

pub(crate) fn new_command(command: &Command, pwm1: &Pwm, pwm2: &Pwm) { 
    match command {
        Command::Go(speed, direction) => go(speed, direction, pwm1, pwm2),
        Command::Stop => stop(pwm1, pwm2),
    }
}

pub(crate) fn init(){
    println!("Init!");
    // stop();
    let mut pin_motor_1_dir = Gpio::new().unwrap().get(GPIO_DIRECTION_M1).unwrap().into_output();
    let mut pin_motor_2_dir = Gpio::new().unwrap().get(GPIO_DIRECTION_M2).unwrap().into_output();
    pin_motor_1_dir.set_high();
    pin_motor_2_dir.set_high();

}

pub(crate) fn stop(pwm1: &Pwm, pwm2: &Pwm) {
    println!("Stop!");
    //Stop PWM channel 1 (duty 0.0)
    // let _pwm = Pwm::with_frequency(Channel::Pwm0, FREQUENCY, 0.0, Polarity::Normal, true).unwrap();
    //Stop PWM channel 2 (duty 0.0)
    // let _pwm = Pwm::with_frequency(Channel::Pwm1, FREQUENCY, 0.0, Polarity::Normal, true).unwrap();
    pwm1.set_duty_cycle(OFF);
    pwm2.set_duty_cycle(OFF);
}

pub(crate) fn go(speed: &Speed, direction: &Direction, pwm1: &Pwm, pwm2: &Pwm) {
    //TODO: talk to the PI GPIO pins
    println!("Go {speed:?}, {direction:?}");

    // match &speed {
    //     Speed::Slow => {}
    //     Speed::Normal => {}
    //     Speed::Fast => {}
    // }

    match &direction {
        Direction::Forward => {
            println!("FWD");
            // Pwm::with_frequency(Channel::Pwm0, FREQUENCY, DUTY_40, Polarity::Normal, true).unwrap();
            // Pwm::with_frequency(Channel::Pwm1, FREQUENCY, DUTY_40, Polarity::Normal, true).unwrap();

            let mut pin_motor_1_dir = Gpio::new().unwrap().get(GPIO_DIRECTION_M1).unwrap().into_output();
            pin_motor_1_dir.set_low();     
            let mut pin_motor_2_dir = Gpio::new().unwrap().get(GPIO_DIRECTION_M2).unwrap().into_output();
            pin_motor_2_dir.set_low();     
            // Sleep for 500 ms while the servo moves into position.
            // thread::sleep(Duration::from_millis(2000));
        
            pwm1.set_duty_cycle(DUTY_30).unwrap();
            pwm2.set_duty_cycle(DUTY_30).unwrap();
        }
        Direction::Left => {}
        Direction::Right => {}
    }
    
}

//     let mut pin = Gpio::new()?.get(GPIO_PWM)?.into_output();

//     // Blink the LED by setting the pin's logic level high for 500 ms.
//     loop {
//     pin.clear_pwm();
//     println!("set frequency");
//     pin.set_pwm_frequency(10000.0, 1.0);
//     thread::sleep(Duration::from_millis(10000));
//     pin.clear_pwm();
//     println!("Clear pwm");
//     return Ok(());
//    // println!(" set pwm");
//    // pin.set_pwm(Duration::from_millis(1000), Duration::from_millis(400));
//    // thread::sleep(Duration::from_millis(10000));
//    // pin.clear_pwm();
//    // println!("Clear pwm");
//     }