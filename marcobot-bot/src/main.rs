mod gpio;

use marcobot_protocol::Command;
use rppal::pwm::{Channel, Polarity, Pwm};
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::thread;
use std::{io::prelude::*, net::TcpStream, time::Duration};

//for signal stuff
use signal_hook::{consts::SIGINT, iterator::Signals};
use std::process;

//test out of scope fix
const FREQUENCY: f64 = 5000.0; // 5kHz
const RECONNET_DURATION: Duration = Duration::from_millis(5000);
const ROBOT_SERVER_IP: &str = include_str!("../../bot_server_ip.txt");

fn run_signals() {
    let mut signals = Signals::new(&[SIGINT]).unwrap();
    for sig in signals.forever() {
        println!("Received signal {:?}", sig);
        if sig.eq(&(2 as i32)) {
            println!("Terminating program, shutting PWM channels down.. (zero duty)");
            Pwm::with_frequency(Channel::Pwm0, FREQUENCY, 0.0, Polarity::Normal, true).unwrap();
            Pwm::with_frequency(Channel::Pwm1, FREQUENCY, 0.0, Polarity::Normal, true).unwrap();
            // gpio::stop();
            process::exit(1);
        }
    }
}

fn init() -> (Pwm, Pwm) {
    gpio::init();
    let pwm1 = Pwm::with_frequency(Channel::Pwm0, FREQUENCY, 0.0, Polarity::Normal, true).unwrap();
    let pwm2 = Pwm::with_frequency(Channel::Pwm1, FREQUENCY, 0.0, Polarity::Normal, true).unwrap();
    (pwm1, pwm2)
}

fn main() {
    let running = Arc::new(AtomicBool::new(true));

    // When a SIGINT (Ctrl-C) or SIGTERM signal is caught, atomically set running to false.
    #[cfg(not(feature = "sim"))]
    thread::spawn(move || run_signals());

    #[cfg(not(feature = "sim"))]
    let (pwm1, pwm2) = init();

    loop {
        if let Ok(mut stream) = TcpStream::connect(ROBOT_SERVER_IP) {
            println!("Connected to: {ROBOT_SERVER_IP}");
            while running.load(Ordering::SeqCst) {
                let mut buffer = [0; 1];
                let _ = stream
                    .read(&mut buffer)
                    .expect("Cannot read buffer stream!");
                if buffer[0] > 0 {
                    let command = Command::from_byte(buffer[0]).unwrap();
                    #[cfg(not(feature = "sim"))]
                    gpio::new_command(&command, &pwm1, &pwm2);
                } else {
                    println!("Error connecting to: {ROBOT_SERVER_IP}");
                    #[cfg(not(feature = "sim"))]
                    gpio::stop(&pwm1, &pwm2);
                    break;
                }
            }
        } else {
            println!("Error connecting to: {ROBOT_SERVER_IP}");
            std::thread::sleep(RECONNET_DURATION);
        }
    }
}
