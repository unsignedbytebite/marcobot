use std::fmt::Display;

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Speed {
    Slow,
    Normal,
    Fast,
}
impl Speed {
    pub fn from_byte(bits: u8) -> Result<Speed, &'static str> {
        match bits {
            1 => Ok(Speed::Slow),
            2 => Ok(Speed::Normal),
            3 => Ok(Speed::Fast),
            _ => Err("Speed Byte code is not supported"),
        }
    }

    pub fn to_byte(&self) -> u8 {
        match self {
            Speed::Slow => 1,
            Speed::Normal => 2,
            Speed::Fast => 3,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Direction {
    Forward,
    Left,
    Right,
}

impl Display for Direction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Direction::Forward => f.write_str("Forward"),
            Direction::Left => f.write_str("Left"),
            Direction::Right => f.write_str("Right"),
        }
    }
}
impl Direction {
    pub fn from_byte(bits: u8) -> Result<Direction, &'static str> {
        match bits {
            1 => Ok(Direction::Forward),
            2 => Ok(Direction::Left),
            3 => Ok(Direction::Right),
            _ => Err("Direction Byte code is not supported"),
        }
    }

    pub fn to_byte(&self) -> u8 {
        match self {
            Direction::Forward => 1,
            Direction::Left => 2,
            Direction::Right => 3,
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum Command {
    Go(Speed, Direction),
    Stop,
}

impl Command {
    pub fn from_byte(command_byte: u8) -> Result<Self, &'static str> {
        if command_byte == 255 {
            Ok(Command::Stop)
        } else {
            let speed_bits = command_byte & 3;
            let direction_bits = (command_byte & 12) >> 2;

            let speed = Speed::from_byte(speed_bits)?;
            let direction = Direction::from_byte(direction_bits)?;

            Ok(Command::Go(speed, direction))
        }
    }

    pub fn to_byte(&self) -> u8 {
        match self {
            Command::Go(speed, direction) => {
                let speed_bits = speed.to_byte();
                let direction_bits = direction.to_byte();

                (direction_bits << 2) | speed_bits
            }
            Command::Stop => 255,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn bits() {
        assert_eq!((12 & 8) >> 2, 2)
    }

    #[test]
    fn command_parsing() {
        let c = Command::from_byte(5).unwrap();
        assert_eq!(c, Command::Go(Speed::Slow, Direction::Forward));
        assert_eq!(c.to_byte(), 5);
        let c = Command::from_byte(10).unwrap();
        assert_eq!(c, Command::Go(Speed::Normal, Direction::Left));
        assert_eq!(c.to_byte(), 10);
        let c = Command::from_byte(6).unwrap();
        assert_eq!(c, Command::Go(Speed::Normal, Direction::Forward));
        assert_eq!(c.to_byte(), 6);
        let c = Command::from_byte(255).unwrap();
        assert_eq!(c, Command::Stop);
        assert_eq!(c.to_byte(), 255);

        let c = Command::from_byte(0);
        assert!(c.is_err());
        let c = Command::from_byte(8);
        assert!(c.is_err());
        let c = Command::from_byte(1);
        assert!(c.is_err());
    }
}
