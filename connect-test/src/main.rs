fn main() {
    const TEST_SERVER_IP: &str = include_str!("../../test_server_ip.txt");
    if std::net::TcpStream::connect(TEST_SERVER_IP).is_ok() {
        println!("😀");
    } else {
        println!("🙁");
    }
}
